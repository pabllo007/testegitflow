package br.com.pabllo007.testeGit.testeGit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteGitApplication.class, args);
	}

}
